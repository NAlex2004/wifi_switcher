package alex.alien.wifiswitcher;

import android.app.Activity;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent settingsIntent = new Intent(this, WifiSwitcherService.class);
        startService(settingsIntent);
        Toast.makeText(this, getString(R.string.service_started), Toast.LENGTH_SHORT).show();

        JobsManager.startRestarterJob(this);

        finish();
    }
}
