package alex.alien.wifiswitcher;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;

class JobsManager {
    private static final int RESTARTER_LATENCY = 3600000; // 1 hour
    private static final int RESTARTER_ID = 1;

    static void startRestarterJob(Context context) {
        JobInfo.Builder jobBuilder = new JobInfo.Builder(RESTARTER_ID, new ComponentName(context, RestarterJobService.class));
        JobInfo jobInfo = jobBuilder
                .setPersisted(true)
                .setPeriodic(RESTARTER_LATENCY)
                .build();
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        scheduler.schedule(jobInfo);
    }

    static void stopRestarterJob(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(RESTARTER_ID);
    }
}
