package alex.alien.wifiswitcher;

import android.app.Activity;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getFragmentManager().beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
    }

    public void stopServiceClick(View view) {
        JobsManager.stopRestarterJob(this);

        Intent stopIntent = new Intent(this, WifiSwitcherService.class);
        stopIntent.putExtra(getString(R.string.need_stop), true);
        startService(stopIntent);
    }

    public void startServiceClick(View view) {
        Intent startIntent = new Intent(this, WifiSwitcherService.class);
        startIntent.putExtra(getString(R.string.need_stop), false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(startIntent);
        } else {
            startService(startIntent);
        }

        JobsManager.startRestarterJob(this);
    }
}
