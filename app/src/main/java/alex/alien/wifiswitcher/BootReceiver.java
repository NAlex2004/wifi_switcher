package alex.alien.wifiswitcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, WifiSwitcherService.class);
        serviceIntent.putExtra(context.getString(R.string.need_stop), false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.i(WifiSwitcherService.LOG_TAG, "BOOT_COMPLETE: startForegroundService..");
            context.startForegroundService(serviceIntent);
        } else {
            Log.i(WifiSwitcherService.LOG_TAG, "BOOT_COMPLETE: startService..");
            context.startService(serviceIntent);
        }

    }
}
