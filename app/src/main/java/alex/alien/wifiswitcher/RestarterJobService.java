package alex.alien.wifiswitcher;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class RestarterJobService extends JobService {
    public RestarterJobService() {
    }

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Context context = getApplicationContext();

        Intent serviceIntent = new Intent(context, WifiSwitcherService.class);
        serviceIntent.putExtra(context.getString(R.string.need_stop), false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.i(WifiSwitcherService.LOG_TAG, "RESTARTER_SERVICE: startForegroundService..");
            context.startForegroundService(serviceIntent);
        } else {
            Log.i(WifiSwitcherService.LOG_TAG, "RESTARTER_SERVICE: startService..");
            context.startService(serviceIntent);
        }

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }
}
