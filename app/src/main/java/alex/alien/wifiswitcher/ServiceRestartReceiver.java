package alex.alien.wifiswitcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class ServiceRestartReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals(context.getString(R.string.restart_service_broadcast))) {
            Intent startIntent = new Intent(context, WifiSwitcherService.class);
            startIntent.putExtra(context.getString(R.string.need_stop), false);
            Log.i(WifiSwitcherService.LOG_TAG, "RESTARTING SERVICE..");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(startIntent);
            } else {
                context.startService(startIntent);
            }
        }
    }
}
