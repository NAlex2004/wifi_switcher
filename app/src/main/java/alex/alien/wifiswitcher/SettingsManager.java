package alex.alien.wifiswitcher;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.preference.PreferenceManager;

class SettingsManager {
    static long getDelayMilliseconds(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String delayStr = preferences.getString(context.getString(R.string.prefKey_Delay), context.getString(R.string.default_delay));
        double delayMinutes = delayStr != null
                ? Double.valueOf(delayStr)
                : 5;

        return (long) (int) Math.round(60000 * delayMinutes);
    }

    static boolean isCharging(Context context) {
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = context.registerReceiver(null, filter);
        if (intent == null) {
            return false;
        }
        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        return status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;
    }
}
