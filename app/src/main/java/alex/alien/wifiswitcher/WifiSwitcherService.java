package alex.alien.wifiswitcher;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import static android.support.v4.app.NotificationCompat.PRIORITY_HIGH;

public class WifiSwitcherService extends Service {
    private static final int ID_SERVICE = 16384;
    private static boolean isWifiOn = false;
    private static boolean isActionScheduled = false;
    private static boolean isScreenOn = false;
    private static int JOB_ID = 0;
    private static WifiManager wifiManager;
    private static JobScheduler jobScheduler;
    private boolean needStop = false;

    public static final String LOG_TAG = "SWITCHER";

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }

            if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                isWifiOn = wifiManager.isWifiEnabled();
                isScreenOn = false;
                Context appContext = getApplicationContext();
                boolean isCharging = SettingsManager.isCharging(appContext);

                if (isCharging) {
                    Log.i(LOG_TAG, "SCREEN_OFF: CHARGING, DO NOTHING");
                    return;
                } else {
                    Log.i(LOG_TAG, "SCREEN_OFF: NOT CHARGING");
                }


                if (isWifiOn && !isActionScheduled) {
                    long delayMs = SettingsManager.getDelayMilliseconds(appContext);

                    ComponentName jobComponentName = new ComponentName(context, SwitchOffWifiJobService.class);
                    JobInfo.Builder jobBuilder = new JobInfo.Builder(JOB_ID, jobComponentName);
                    JobInfo job = jobBuilder.setMinimumLatency(delayMs)
                                    .setPersisted(false)
                                    .build();

                    jobScheduler.schedule(job);
                    isActionScheduled = true;

                    Log.i(LOG_TAG, "SCREEN_OFF: SCHEDULE JOB");
                }
            } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
                isScreenOn = true;
                if (isWifiOn) {
                    wifiManager.setWifiEnabled(true);
                    Log.i(LOG_TAG, "SCREEN_ON: SET WIFI ON");
                }
                if (isActionScheduled) {
                    isActionScheduled = false;
                    jobScheduler.cancel(JOB_ID);
                    Log.i(LOG_TAG, "SCREEN_ON: CANCEL JOB");
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "SERVICE DESTROYED..");
        unregisterReceiver(receiver);
        if (!needStop) {
            Log.i(LOG_TAG, "SENDING BROADCAST TO RESTART SERVICE");
            Intent restartIntent = new Intent(this, ServiceRestartReceiver.class);
            sendBroadcast(restartIntent);
        }
        super.onDestroy();
    }

    public WifiSwitcherService() {
    }

    private void initReceiver() {
        IntentFilter intents = new IntentFilter();
        intents.addAction(Intent.ACTION_SCREEN_ON);
        intents.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(receiver, intents);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        jobScheduler = (JobScheduler) getApplicationContext().getSystemService(Context.JOB_SCHEDULER_SERVICE);

        Intent mainActivityIntent = new Intent(this, SettingsActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(mainActivityIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "";
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle(getString(R.string.title))
                .setSmallIcon(R.drawable.ic_wifi_switcher)
                .setPriority(PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setContentIntent(pendingIntent)
                .build();
        initReceiver();
        startForeground(ID_SERVICE, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            needStop = intent.getBooleanExtra(getString(R.string.need_stop), false);
            if (needStop) {
                stopSelf();
            }
        }

        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager){
        String channelId = "WifiSwitcherServiceChannel_ID_65432";
        String channelName = "WifiSwitcherServiceChannel";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }

    // JobService
    public static class SwitchOffWifiJobService extends JobService {
        @Override
        public boolean onStartJob(JobParameters jobParameters) {
            Log.i(LOG_TAG, "JOB: STARTED");

            boolean isCharging = SettingsManager.isCharging(getApplicationContext());
            if (isCharging) {
                Log.i(LOG_TAG, "JOB: BATTERY IS CHARGING, DO NOTHING");
                return false;
            }

            if (isWifiOn && !isScreenOn ) {
                wifiManager.setWifiEnabled(false);
                Log.i(LOG_TAG, "JOB: SET WIFI OFF");
            }
            isActionScheduled = false;

            return false;
        }

        @Override
        public boolean onStopJob(JobParameters jobParameters) {
            return false;
        }
    }
}
