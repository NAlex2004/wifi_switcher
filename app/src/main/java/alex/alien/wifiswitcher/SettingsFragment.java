package alex.alien.wifiswitcher;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import alex.alien.wifiswitcher.R;

public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
